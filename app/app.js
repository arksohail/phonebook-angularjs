var myPhoneApp = angular.module('myPhoneApp', ['ngRoute']);

myPhoneApp.config(['$routeProvider', function($routeProvider){

  $routeProvider
    .when('/home', {
    templateUrl: 'views/home.html',
  })
    .when('/directory', {
    templateUrl: 'views/directory.html',
    controller: 'PhoneController'
  })
    .otherwise({
    redirectTo: '/home'
  });

}]);

myPhoneApp.controller('PhoneController', ['$scope', '$http', function($scope, $http){

  // $scope.message = 'Hello World!';

  // $scope.persons = [
  //   {
  //     firstName: 'Daffy',
  //     lastName: 'Jr',
  // 		number: 9113665762,
  // 		thumb: 'http://svgur.com/i/65U.svg'
  // 	},
  // 	{
  //     firstName: 'Joey',
  //     lastName: 'Tribbiani',
  // 		number: 7204114475,
  // 		thumb: 'http://svgur.com/i/65U.svg'				
  // 	},
  // 	{
  //     firstName: 'Chandler',
  //     lastName: 'Bing',
  //     number: 7265984575,
  // 		thumb: 'http://svgur.com/i/65U.svg'
  // 	},
  // ];

  // Converting the Array into JSON
  // console.log(angular.toJson($scope.persons));

  // Remove a Person
  $scope.removePerson = function(person) {

    var removedPerson = $scope.persons.indexOf(person);
    $scope.persons.splice(removedPerson, 1);

  };

  // Add a Person
  $scope.addPerson = function(){
    $scope.persons.push({
      firstName: $scope.newPerson.firstName,
      email: $scope.newPerson.email,
      number: $scope.newPerson.number,
	  thumb: 'http://svgur.com/i/65U.svg',
	  profile: 0
    });

    // Emptying Form Input
    // $scope.newPerson.firstName = '';
    // $scope.newPerson.email = '';
    // $scope.newPerson.number = '';
			
  };

  // Updating aan existing entry
  $scope.updatePerson = function(person){
    var updatedPerson = $scope.persons.indexOf(person);
    var fname = document.getElementById('fname').value;
    var email = document.getElementById('email').value;
    var numb = document.getElementById('numb').value;
    $scope.persons[updatedPerson]['firstName'] = fname;
    $scope.persons[updatedPerson]['email'] = email;
    $scope.persons[updatedPerson]['number'] = numb;
    // console.log(name);
  };

  // Getting the Data using Http Request
  $http.get('data/phone.json')
    .then(function(response){
    $scope.persons = response.data;
  });

  // Toggle directive on click
  $scope.toggle = function(person) {
    var ind = $scope.persons.indexOf(person);
    $scope.persons[ind]['profile'] = !$scope.persons[ind]['profile'];
  };

}]);